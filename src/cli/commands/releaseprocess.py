#!/usr/bin/python3
# ******************************************************************************
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
# licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.
# ******************************************************************************/

from cli.base import BaseCommand
# from application.start_process import StartProcess

class ReleaseProcessCommand(BaseCommand):

    def __init__(self):
        super(ReleaseProcessCommand, self).__init__()

        self.sub_parse = BaseCommand.subparsers.add_parser(
            'release', help="the final process for releasing, get the checkok command from release managers")
        # self.collection = True
        self.params = [
            self.sub_args('releaseIssueID', 'start release process issue ID', True, 'store', None, None, None),
            self.sub_args('--giteeid', 'the Gitee ID who trigger this command', True, 'store', None, True, None),
            self.sub_args('--type', 'Specify the release check type, only allow checkok and cvrfok', True, 'store', None, True, ['checkok','cvrfok'])
        ]

    def register(self):
        super(ReleaseProcessCommand, self).register()
        # for cmd_params in self.collection_params:
        #     self.sub_parse.add_argument(
        #         cmd_params[0], nargs='*', default=None, help=cmd_params[1]
        #     )
        # self.sub_parse.set_defaults(func=self.do_command)

    def do_command(self, params):
        issueID = params.releaseIssueID
        check_type = params.type
        print("release process", issueID, check_type)
        # start_process = ReleaseProcessCommand.get_pkg_list()
